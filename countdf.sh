#!/bin/bash
path=$1

echo "The ${path} directory contains:"

# variable to count the number of empty files
empty_files=0

# variable to count the number of non-empty files
non_empty_files=0

# variable to count the number of empty directories
empty_directories=0

# variable to count the number of non-empty directories
non_empty_directories=0


# go through each file in the directory
for i in $path/*
do
    
    # this is a directory
    if [ -d "$i" ]; then

        # non-empty directory, increase count by 1
        if [ "$(ls -A "$i")" ]; then
            let non_empty_directories=$non_empty_directories+1 

        # empty directory, increase count by 1
        else
            let empty_directories=$empty_directories+1
        fi

    # this file is non-empty
    elif [ -s "$i" ]; then
        let non_empty_files=$non_empty_files+1

       # empty file
    else
        let empty_files=$empty_files+1
    fi
done

#print the result
echo "${non_empty_files} files that contain data"
echo "${empty_files} files that are empty"
echo "${non_empty_directories} non-empty directories"
echo "${empty_directories} empty directories"

exit 0 
