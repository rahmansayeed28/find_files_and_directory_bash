# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Bash Practice (problem details given below)

### Write a script that counts the number of files and directories within a given directory according to their properties.
### Details:
*	Call the script countdf.sh
*	When the script is run, the path to a directory will be used by your tutor at the command line, e.g. ./countdf.sh ~/CSI6203/backups/june2020. This is an example path only, so do not hard-code it into your script.
*	The output of the script to the terminal will appear as follows:  
The [dirname] directory contains:  
x files that contain data  
x files that are empty  
x non-empty directories  
x empty directories where [dirname] represents the directory of your tutor’s choosing and x represents the number of files and directories found matching each criterion.
*	Be sure that the script can be run from any directory it is placed in within anyone's Linux user directory structure.
*	There is no need for data validation in this script; your tutor will enter a full path/directory that does exist and within which a range 
